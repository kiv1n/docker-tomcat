#!/bin/bash

IMAGE_NAME=tomcat
CONTAINER_NAME=tomcat
PORT=80

sudo docker build -t $IMAGE_NAME .
sudo docker run -d -p $PORT:$PORT --name $IMAGE_NAME $CONTAINER_NAME